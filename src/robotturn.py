#! /usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from math import * 


x = 0.0
y = 0.0
theta = 0.0
ygoal = 5

def newOdom(msg):
	global x
	global y
	global theta
	global ygoal

	x = msg.pose.pose.position.x
	y = msg.pose.pose.position.y 
	ygoal = y
	rot_q = msg.pose.pose.orientation
	(roll,pitch,theta) = euler_from_quaternion([rot_q.x,rot_q.y,rot_q.z,rot_q.w]) 





rospy.init_node("speed_controller")

speed = Twist()

goal = Point()


sub = rospy.Subscriber("/robot1/odom",Odometry,newOdom) 
pub = rospy.Publisher("/robot1/mobile_base/commands/velocity",Twist,queue_size = 1)
r = rospy.Rate(10)
#print(inc_y)

goal.x = 0
goal.y = 5
print("Going Straight")

while not rospy.is_shutdown():
	if (y > 3.6):
		goal.x =  3
		goal.y =  4
	print("Right Turn")
	inc_x = goal.x - x 
	inc_y = goal.y - y
	print(ygoal)

	angle_to_goal = atan2(inc_y,inc_x)

	if abs(angle_to_goal - theta) > 0.1:
		speed.linear.x = 0.01
		speed.angular.z = -0.4

	else:
		speed.linear.x = 0.5
		speed.angular.z = 0


	pub.publish(speed)
	r.sleep()
