#! /usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from math import * 


x = 0.0
y = 0.0
theta = 0.0

def newOdom(msg):
    global x
    global y
    global theta

    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y

    rot_q = msg.pose.pose.orientation
    (roll,pitch,theta) = euler_from_quaternion([rot_q.x,rot_q.y,rot_q.z,rot_q.w]) 





rospy.init_node("speed_controller2")

speed = Twist()

goal = Point()
goal.x = 13
goal.y = 0

sub2 = rospy.Subscriber("/robot2/odom",Odometry,newOdom) 
pub2 = rospy.Publisher("/robot2/mobile_base/commands/velocity",Twist,queue_size = 1)
r = rospy.Rate(10)

while not rospy.is_shutdown():
    inc_x = goal.x - x 
    inc_y = goal.y - y

    angle_to_goal = atan2(inc_y,inc_x)

    if abs(angle_to_goal - theta) > 0.1:
        speed.linear.x = 0.02
        speed.angular.z = 0.4

    else:
        speed.linear.x = 0.8
        speed.angular.z = 0

    pub2.publish(speed)
    r.sleep()
